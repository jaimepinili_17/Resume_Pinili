<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gallery</title>
</head>

<link rel="stylesheet" href="gallery.css">
<link rel="stylesheet" href="returnbutton.css">

<body>
    <div class="gallery">
        <a target="_blank" href="images/FB_IMG_1521984499699.jpg">
        <img src="images/FB_IMG_1521984499699.jpg" alt="1521" width="500" height="300">
        </a>
    </div>
     <div class="gallery">
        <a target="_blank" href="images/FB_IMG_1521987208144.jpg">
        <img src="images/FB_IMG_1521987208144.jpg" alt="8144" width="300" height="300">
        </a>
    </div>
    <div class="gallery">
        <a target="_blank" href="images/FB_IMG_1521987213624.jpg">
        <img src="images/FB_IMG_1521987213624.jpg" alt="3624" width="300" height="300">
        </a>
    </div>
    <div class="gallery">
        <a target="_blank" href="images/KUJICAM_2018-05-11-09-32-40_developed.jpg">
        <img src="images/KUJICAM_2018-05-11-09-32-40_developed.jpg" alt="40" width="300" height="300">
        </a>
    </div>
    <div class="gallery">
        <a target="_blank" href="images/KUJICAM_2018-05-11-09-33-28_developed.jpg">
        <img src="images/KUJICAM_2018-05-11-09-33-28_developed.jpg" alt="40" width="300" height="300">
        </a>
    </div>
    <div class="gallery">
        <a target="_blank" href="images/nebi_201819213.jpg">
        <img src="images/nebi_201819213.jpg" alt="213" width="500" height="300">
        </a>
    </div>
    <div class="button">
        <a href="Resume.html"><p>Return</p></a>
    </div>
</body>
</html>
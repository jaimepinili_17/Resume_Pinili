<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>educationalattainment</title>
</head>

<link rel="stylesheet" href="edattainment.css">
<link rel="stylesheet" href="returnbutton.css">

<body>
    <div class="educat">
        <table>
                <tr>
                    <th>School</th> 
                    <th>Address</th>
                    <th>Year Graduated</th>
                </tr>
                <tr>
                    <td>San Isidro Elementary School</td>
                    <td>Brgy. San Isidro, Kaputian, IGACOS</td>
                    <td>2010 - 2011</td>
                </tr>
                <tr>
                    <td>Sta. Ana National High School</td>
                    <td>D. Suazo St., DC</td>
                    <td>2011 - 2015</td>
                </tr>
                <tr>
                    <td>University of Southeastern Philippines</td>
                    <td>Inigo St., Bo. Obrero, DC</td>
                    <td>2015 - Present</td>
                </tr>
        </table>
    </div>
    <div class="button">
        <a href="Resume.html"><p>Return</p></a>
    </div>
</body>
</html>
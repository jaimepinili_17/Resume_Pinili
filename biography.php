<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biography</title>
</head>

<link rel="stylesheet" href="biography.css">
<link rel="stylesheet" href="returnbutton.css">

<body>
    <div class="bio">
        <table>
            <tr>
                <th>Biography</th>
            </tr>
            <tr>
                <td>Name : </td>
                <td>Jaime Gabutero Pinili Jr.</td>
            </tr>
            <tr>
                <td>Age : </td>
                <td>19 Years Old</td>

            </tr>
            <tr>
                <td>Gender : </td>
                <td>Male</td>
            </tr>
            <tr>
                <td>Date of birth : </td>
                <td>January 23, 1999</td>
            </tr>
            <tr>
                <td>Bloodtype : </td>
                <td> O+ </td>
            </tr>
            <tr>
                <td>Address : </td>
                <td>Purok 12-B, St. John, Bucana, Davao City </td>
            </tr>
            <tr>
                <td>Province : </td>
                <td>Davao Del Sur</td>
            </tr>
            <tr>
                <td>Zip Code : </td>
                <td>8000</t>
            </tr>
            <tr>
                <td>Tin no. : </td>
                <td>--</t>
            </tr>
            <tr>
                <th>Mother</th>
            </tr>
            <tr>
                <td>Name : </td>
                <td>Avelina G. Pinili</td>
            </tr> 
            <tr>
                <td>Age : </td>
                <td>52 Years old</td>
            </tr>
            <tr>
                <td>Occupation : </td>
                <td>Housewife</td>
            </tr>
            <tr>
                <th>Father</th>
            </tr>
            <tr>
                <td>Name : </td>
                <td>Jaime G. Pinili Sr.</td>
            </tr> 
            <tr>
                <td>Age : </td>
                <td>62 Years old</td>
            </tr>
            <tr>
                <td>Occupation : </td>
                <td>Laborer</td>
            </tr>
        </table>
    </div>
    <div class="button">
        <a href="Resume.html"><p>Return</p></a>
    </div>
</body>
</html>
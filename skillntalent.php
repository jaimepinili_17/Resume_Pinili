<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SkillsandTalent</title>
</head>

<link rel="stylesheet" href="skillntalent.css">
<link rel="stylesheet" href="returnbutton.css">

<body>
    <div class="sat">
        <table>
            <tr>
                <th>Skills</th>
            </tr>
            <tr>
                <td>Can work under pressure</td>
            </tr>
            <tr>
                <td>Knowledgeable about some of programming languages</td>
            </tr>
            <tr>
                <td>Can handle/manage people</td>
            </tr>
            <tr>
                <td>Can organize plans</td>
            </tr>
            <tr>
                <th>Talents</th>
            </tr>
            <tr>
                <td>Singing</td>
            </tr>
            <tr>
                <td>Drawing</td>
            </tr>
        </table>
    </div>
    <div class="button">
        <a href="Resume.html"><p>Return</p></a>
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Resume</title>
</head>

<link rel="stylesheet" href="topnav.css">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="homebuttons.css">

<body>
    <div class="topnav">
        <span><h1 class="res">RESUME</h1></span>
        <a href="gallery.html">Gallery</a>
        <a href="about.html">About</a>
    </div>

    <div class="header">
        <a target="_blank" href="images/IMG_20180823_230309.jpg">
            <img class="propic" src="images/IMG_20180823_230309.jpg" alt="309">
        </a>
        <p class="tname">Jaime Gabutero Pinili Jr.</p>
        <p class="tsubname">Student | <span>U</span>niversity of <span>S</span>outh<span>e</span>astern <span>P</span>hilippines</p>
        <p class="paragraph">Hi ! Welcome to my personal Webpage.</p> 
        <p class="paragraph1"> Click those buttons below to know me better....</p>
    </div>

    <div class="homebuttons">
        <a href="biography.html"><p>Biography</p></a>
        <a href="edattainment.html"><p>Educational Attainment</p></a>
        <a href="skillntalent.html"><p>Skills and Talent</p></a>
    </div>
</body>
</html>